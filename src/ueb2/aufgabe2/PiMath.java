package ueb2.aufgabe2;

public class PiMath {

	public static void main(String args[]) {
		double radius = 3.0d;
	
		// Code f�r Radiuseingabe via Kommandozeile
		if (args.length == 1) {
			radius = Double.parseDouble(args[0]);
		}
		
		System.out.println("Radius: " + radius);
		System.out.println("Kreisfl�che: " + getCircleArea(radius));
		System.out.println("Kreisumfang: " + getCirclePerimeter(radius));
		System.out.println("Kugelvolumen: " + getSphereVolume(radius));
		
	}
	
	public static double getCircleArea(double radius) {
		return Math.PI * radius * radius; 
	}
	
	public static double getCirclePerimeter(double radius) {
		return 2.0d * Math.PI * radius;
	}
	
	public static double getSphereVolume(double radius) {
		return 4.0d * Math.PI * radius * radius * radius / 3.0d;
	}
	
}
