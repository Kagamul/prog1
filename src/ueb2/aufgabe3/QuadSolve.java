package ueb2.aufgabe3;

public class QuadSolve {

	public static void main(String[] args) {
		
		double p = 1.0d, q = 0.0d;
	
		// Code f�r Eingabe via Kommandozeile
		if (args.length == 2) {
			p = Double.parseDouble(args[0]);
			q = Double.parseDouble(args[1]);
		}
		
		System.out.println("p: " + p);
		System.out.println("q: " + q);
		
		double[] solution = solve(p,q);
		
		switch (solution.length) {
		case 0:
			System.out.println("Keine L�sung f�r die Gleichung x*x + p*x + q = 0 in R!");
			break;
		case 1:
			System.out.println("x = " + solution[0]);
			break;
		case 2:
			System.out.println("x1 = " + solution[0]);
			System.out.println("x2 = " + solution[1]);
			break;
		default:
			System.err.println("Die Gleichung x*x + p*x + q = 0 lieferte mehr als 2 L�sungen!");
			for (int i = 1; i <= solution.length; i++) {
				System.err.println("x"+i+" = "+solution[i-1]);
			}
			break;
		}
		
	}
	
	public static double[] solve(double p, double q) {
		double d = p / 2.0d;
		double det = d * d - q;
		double ret[];
		if (det == 0.0d) {
			ret = new double[1];
			ret[0] = - d;
			return ret;
		}
		if (det < 0.0d)
			return new double[0];
		ret = new double[2];
		det = Math.sqrt(det);
		ret[0] = - d + det;
		ret[1] = - d - det;
		return ret;
	}
	
}
