package ueb2.aufgabe4;

public class TimeMath {
	
	final static long secondsPerDay = 60L * 60L * 24L;

	public static void main(String[] args) {
		int sekunden = getCurrentSeconds();
		int minuten = getCurrentMinutes();
		int stunden = getCurrentHours();
		
		// Code f�r Eingabe via Kommandozeile
		if (args.length == 3) {
			stunden = Integer.parseInt(args[0]);
			minuten = Integer.parseInt(args[0]);
			sekunden = Integer.parseInt(args[0]);
		}
		
		// Werte-Normalisierung
		minuten += sekunden / 60;
		stunden += minuten / 60;
		sekunden %= 60;
		minuten %= 60;
		stunden %= 24;
		
		System.out.println(stunden+" : "+minuten+" : "+ sekunden);
		System.out.println("Sekunden seit Mitternacht: " + secondsPassed(stunden, minuten, sekunden));
		System.out.println("Sekunden bis Mitternacht: " + secondsToMidnight(stunden, minuten, sekunden));
		System.out.println(pctPassed(stunden, minuten, sekunden)+"% des Tages sind vergangen");
		
	}
	
	private static int getCurrentHours() {
		long millis = System.currentTimeMillis();
		long hours = millis / (1000L * 60L * 60L);
		return (int)(hours % 24) + 2;
	}
	
	private static int getCurrentMinutes() {
		long millis = System.currentTimeMillis();
		long minutes = millis / (1000L * 60L);
		return (int)(minutes % 60);
	}
	
	private static int getCurrentSeconds() {
		long millis = System.currentTimeMillis();
		long seconds = millis / (1000L);
		return (int)(seconds % 60);
	}
	
	public static long secondsPassed(long hours, long minutes, long seconds) {
		return hours * 60L * 60L + minutes * 60L + seconds;
	} 
	
	public static long secondsToMidnight(long hours, long minutes, long seconds) {
		long secsPassed = secondsPassed(hours, minutes, seconds);
		secsPassed %= secondsPerDay;
		return secondsPerDay - secsPassed;
	}
	
	public static double pctPassed(long hours, long minutes, long seconds) {
		long secsPassed = secondsPassed(hours, minutes, seconds);
		return 100.0d * (double) secsPassed / (double) secondsPerDay;
	}
	
}
