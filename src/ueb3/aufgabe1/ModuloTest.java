package ueb3.aufgabe1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ModuloTest {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String sysIn = "";
		int n = 0;
		while (true) {
			System.out.println("Enter an integer or 'exit' to close the program: ");
			sysIn = br.readLine().trim();
			if (sysIn.equalsIgnoreCase("exit"))
				return;
			try{
				n = Integer.parseInt(sysIn);
				System.out.println(n+" % 7 = " + (((n % 7) == 0) ? "true" : "false"));
			}catch(NumberFormatException e){
				System.out.println("'"+sysIn+"' is not an integer!");
			}
		}
	}
	
}
