package ueb3.aufgabe2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PizzaTest {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		double[] values = new double[4];
		String sysIn, type;
		boolean next;
		int num;
		for (int i = 0; i < 4; i++) {
			next = false;
			while (!next) {
				num = (i / 2) % 2 + 1;
				type = (i % 2 == 0) ? "diameter" : "price";
				System.out.println("Enter "+type+" for Pizza #"+num);
				sysIn = br.readLine();
				try {
					values[i] = Double.parseDouble(sysIn);
					if (values[i] > 0)
						next = true;
					else
						System.out.println("Enter number bigger than 0!");
				} catch (NumberFormatException e) {
					System.out.println("Not a number!");
				}
			}
		}
		values[0] *= values[0] * Math.PI * 0.25;
		values[2] *= values[2] * Math.PI * 0.25;
		num = (values[1] / values[0] > values[3] / values[2]) ? 2 : 1;
		System.out.println("Pizza #"+num+" is more cost efficient.");
	}
	
}
