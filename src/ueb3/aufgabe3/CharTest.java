package ueb3.aufgabe3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CharTest {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String sysIn;
		Character c;
		int num;
		while (true) {
			System.out.println("Enter a String or 'exit' to close.");
			sysIn = br.readLine();
			if (sysIn.trim().equalsIgnoreCase("exit"))
				return;
			for (int i = 0; i < sysIn.length(); i++) {
				c = sysIn.charAt(i);
				num = 0;
				System.out.println("Character '"+c+"':");
				if (Character.isUpperCase(c))
					System.out.println(++num+": Großbuchstabe");
				if ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9'))
					System.out.println(++num+": Hexadezimale Ziffer");
				if (c >= '0' && c <= '7')
					System.out.println(++num+": oktale Ziffer");
				if (c >= '0' && c <= '1')
					System.out.println(++num+": binäre Ziffer");
				if (num == 0)
					System.out.println("Unbekannt");
			}
		}
	}
	
}
