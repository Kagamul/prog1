package ueb4.aufgabe1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FreeFall {

	private static final double g = 9.89665;
	private static final int step = 5;
	
	public static void main(String[] args) throws InterruptedException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int duration;
		int t = 0;
		double meter;
		while (true) {
			try {
				System.out.println("Bitte Falldauer in Sekunden eingeben:");
				duration = Integer.parseInt(br.readLine());
				if (duration < 1)
					continue;
			} catch (Exception e) {
				continue;
			}
			break;
		}
		while (t < (duration)) {
			Thread.sleep(1000*step);
			t+=step;
			meter = t*t*g/2d;
			System.out.println("Zeit: "+t+" Sekunden: Zur�ckgelegte Strecke: "+meter+"m");
		}
	}

}
