package ueb4.aufgabe2;

public class Sum {

	public static void main(String[] args) {
		double diff = 1;
		double oldSum, currentSum = 6;
		int k = 1;
		while (diff >= 0.00001d) {
			System.out.println("Step "+k+": "+currentSum);
			k++;
			oldSum = currentSum;
			currentSum +=  6d/(k*k);
			diff = currentSum - oldSum;
		}
		System.out.println("Step "+k+": "+currentSum);
	}

}
