package ueb4.aufgabe4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class IsPrime {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int prime;
		while (true) {
			try {
				System.out.println("Bitte zu untersuchende Zahl eingeben:");
				prime = Integer.parseInt(br.readLine());
				if (prime < 1)
					continue;
			} catch (Exception e) {
				continue;
			}
			break;
		}
		for (int i = 2; i < prime; i++) {
			if (prime % i == 0) {
				System.out.println("Untersuchte Zahle ist keine Primzahl!");
				return;
			}
		}
		System.out.println("Untersuchte Zahl ist eine Primzahl!");
	}

}
