package ueb5.aufgabe1;

public class Temperaturumwandlung {

	public static void main(String[] args) {
		float celsius;
		System.out.println("Fahrenheit\tCelsius");
		for (int fahrenheit = 0; fahrenheit <301; fahrenheit++) {
			celsius = (int)(5f*(fahrenheit -32)*100/9f) /100f;
			System.out.println(fahrenheit + "\t\t" + celsius);
		}
	}

}
