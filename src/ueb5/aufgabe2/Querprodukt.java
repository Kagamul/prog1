package ueb5.aufgabe2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Querprodukt {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int i;
		int multDigRoot;
		String lineIn;
		while (true) {
			System.out.println("Enter an Integer between 0 and 1.000.000 or exit:");
			try {
				lineIn = br.readLine();
				if (lineIn.trim().equalsIgnoreCase("exit"))
					return;
				i = Integer.parseInt(lineIn);
				if (i < 0 || i > 1000000)
					throw new Exception();
				multDigRoot = 1;
				for (Character c : (i+"").toCharArray())
					multDigRoot *= Character.getNumericValue(c);
				System.out.println("The multiplicative digital root of "+i+" is "+multDigRoot);
			} catch (Exception e) {
				System.out.println("Invalid Input!");
			}
		}
	}
	
}
