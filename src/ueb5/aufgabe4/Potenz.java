package ueb5.aufgabe4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Potenz {

	public static void main(String[] args) {
		double base, result;
		int exponent;
		boolean invert;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Enter base: ");
			base = Double.parseDouble(br.readLine());
			System.out.println("Enter exponent: ");
			exponent = Integer.parseInt(br.readLine());
		} catch (Exception e) {
			System.out.println("Invalid Input!");
			new Thread(() -> main(args)).run(); // Java 8 required
			return;
		}
		
		if (exponent == 0)
			result = 1d;
		else {
			invert = exponent < 0;
			exponent *= invert ? -1 : 1;
			boolean first = true;
			double ret = 1d;
			double curr = 1d;
			int shift = 1;
			while (exponent - shift >= 0) {
				curr *= first ? base : curr;
				if ((shift & exponent) > 0)
					ret *= curr;
				first = false;
				shift <<= 1;
			}
			
			result = invert ? 1d/ret : ret;
			exponent *= invert ? -1 : 1;
		}
		System.out.println(base+"^"+exponent+" = "+result);
	}

}
