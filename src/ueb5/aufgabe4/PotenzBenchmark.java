package ueb5.aufgabe4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PotenzBenchmark {
	
	public static void main(String[] args) {
		int runs1 = 1000;
		int runs2 = 10000;
		Random random = new Random();
		long diff[] = new long[2];
		List<Double> l = new ArrayList<Double>();
		long t1, t2;
		Statistics[] stats = new Statistics[2];
		
		t1 = System.currentTimeMillis();
		for (int i = 0; i < runs1; i++) {
			t2 = System.currentTimeMillis();
			for (int j = 0; j < runs2; j++)
				Math.pow(random.nextDouble(), random.nextInt());
			l.add((double)(System.currentTimeMillis() - t2));
		}
		diff[0] = System.currentTimeMillis() - t1;
		stats[0] = new Statistics(l);
		
		l = new ArrayList<Double>();
		
		t1 = System.currentTimeMillis();
		for (int i = 0; i < runs1; i++) {
			t2 = System.currentTimeMillis();
			for (int j = 0; j < runs2; j++)
				exponentiate(random.nextDouble(), random.nextInt());
			l.add((double)(System.currentTimeMillis() - t2));
		}
		diff[1] = System.currentTimeMillis() - t1;
		stats[1] = new Statistics(l);
		
		System.out.println("Math.pow : "+diff[0]);
		System.out.println("Mean: "+stats[0].getMean());
		System.out.println("Variance: "+stats[0].getVariance());
		System.out.println("StdDev: "+stats[0].getStdDev());
		System.out.println();
		System.out.println("exponentiate: "+diff[1]);
		System.out.println("Mean: "+stats[1].getMean());
		System.out.println("Variance: "+stats[1].getVariance());
		System.out.println("StdDev: "+stats[1].getStdDev());
	}
	
	public static Double exponentiate(double base, int exponent) {
		boolean invert = exponent < 0;
		exponent *= invert ? -1 : 1;
		if (exponent == 0)
			return 1d;
		int b = 0;
		double ret = 1d;
		double curr = 1d;
		int shift = 1;
		while (exponent - shift >= 0) {
			curr *= b == 0 ? base : curr;
			if ((shift & exponent) > 0)
				ret *= curr;
			b++;
			shift <<= 1;
		}
		
		return invert ? 1d/ret : ret;
	}
	
}
