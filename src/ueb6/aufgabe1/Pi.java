package ueb6.aufgabe1;

import java.io.*;

public class Pi {

	public static void main(String[] args) {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Anzahl der Rechtecke eingeben:");
		int n;
		try {
			n = Integer.parseInt(br.readLine());
			if (n < 1)
				throw new Exception();
		} catch (Exception e) {
			System.out.println("Ung�ltige Eingabe!");
			new Thread(() -> main(args)).start();
			return;
		}
		
		double step = 1d / n;
		double pi = 0d;
		for (int x = 0; x < n; x++)
			pi += step * Math.sqrt(1-(x*step)*(x*step));
		pi *= 4;
		
		System.out.println("Pi = "+pi);
		
	}

}
