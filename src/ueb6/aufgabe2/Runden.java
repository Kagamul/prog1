package ueb6.aufgabe2;

public class Runden {
	
	public static void main(String[] args) {
		System.out.println(rundena(-3.455d));
	}

	public static double rundena(double d) {
		double diff = d - (int) d;
		if (diff < 0) diff *= -1;
		if (diff == 0d)
			return d;
		if (diff < 0.5d)
			return (double)(int)d;
		return (double)(int)( d < 0 ? d-1 : d+1);
	}
	
	public static Double rundenb(double d, int stellen) {
		if (stellen < 0)
			return null;
		double magnitude = 1;
		for (int i = 1 ; i < stellen+1; i++)
			magnitude *= 10;
		d *= magnitude;
		double diff = d - (int) d;
		if (diff < 0) diff *= -1;
		if (diff == 0d)
			return d / magnitude;
		if (diff < 0.5d)
			return (double)(int)d / magnitude;
		return  d < 0 ? ((double)(int)(d-1)) / magnitude : ((double)(int)(d+1)) / magnitude;
	}
	
}
