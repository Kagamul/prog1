package ueb6.aufgabe3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Fakultaet {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String lineIn;
		int n;
		while (true) {
			System.out.println("Enter an Integer or exit to close.");
			lineIn = br.readLine().trim();
			if (lineIn.equalsIgnoreCase("exit"))
				break;
			try {
				n = Integer.parseInt(lineIn);
				if (n < 0)
					throw new Exception();
				System.out.println(n+"! = "+factorial(n));
			} catch (Exception e) {
				System.out.println("Invalid input!");
			}
		}
		br.close();
	}
	
	public static BigInteger factorial(int n) {
		if (n < 0)
			return null;
		BigInteger result = BigInteger.valueOf(1);
		for (int i = n; i > 1; i--)
			result = result.multiply(BigInteger.valueOf(i));
		return result;
	}
	
}
