package ueb6.aufgabe4;

public class Dreieck {

	public static void main(String[] args) {
		printTriangle(30);
	}
	
	public static void printTriangle(int size) {
		for (int i = 0; i < size; i++)
			System.out.println(new String(new char[size - i -1]).replace("\0", " ")
					+ new String(new char[i+1]).replace("\0", "* "));
	}
	
}
