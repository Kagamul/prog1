package ueb7;

public class Aufgabe1 {

	public static void main(String[] args) {
		double base = SysInHandler.get(Double.class, "Enter base:");
		int exp = SysInHandler.get(Integer.class, "Enter exponent:");
		
		System.out.println(base+"^"+exp+" = "+exponentiate(base, exp));
	}
	
	public static Double exponentiate(double base, int power) {
		if (power < 0)
			return null;
		return power == 0 ? 1 : exponentiate(base, power-1) * base;
	}

}
