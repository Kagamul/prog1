package ueb7;

public class Aufgabe2 {

	public static void main(String[] args) {
		double x = SysInHandler.get(Double.class, "Enter a number");
		System.out.println("Sin("+x+") = "+sin(x));
	}
	
	public static double sin(double x) {
		x %= 2*Math.PI;
		double nominator = x;
		double denominator = 1d;
		double sum = x;
		double step = 1;
		for (int i = 2; step > 1E-15 || step < -1E-15 ;i+=2) {
			nominator *= x*x;
			denominator *= i*i + i;
			step = (i % 4 == 0 ? 1 : -1) * nominator / denominator;
			sum += step;
		}
		return sum;
	}

}
