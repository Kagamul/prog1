package ueb7;

public class Aufgabe3u4 {

	public static void main(String[] args) {
		int size = SysInHandler.get(Integer.class, "How many values do you want to enter?:");
		if (size < 1)
			size = 1;
		double[] values = new double[size];
		double minValue = Double.MAX_VALUE, maxValue = 0;
		for (int i = 0; i < size; i++) {
			values[i] = getValue(i+1);
			if (values[i] < minValue)
				minValue = values[i];
			if (values[i] > maxValue)
				maxValue = values[i];
		}
		System.out.println("min value: "+minValue);
		System.out.println("max value: "+maxValue);
		System.out.println("mean: "+mean(values));
		System.out.println("standard deviation: "+stdDev(values));
	}
	
	private static double getValue(int i) {
		double ret = SysInHandler.get(Double.class,i+ ". Value:");
		return ret;
	}
	
	public static double mean(double[] values) {
		double sum = 0;
		for (double value : values)
			sum += value;
		return sum / values.length;
	}
	
	public static double stdDev(double[] values) {
		double mean = mean(values);
		double sum = 0;
		for (double value : values)
			sum += (value - mean) * (value - mean);
		return Math.sqrt(sum / values.length);
	}
	
}
