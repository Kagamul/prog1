package ueb8;

import java.util.Arrays;
import java.util.List;

public class Aufgabe2 {
	
	public static void main(String[] args) {
		List<Character> vocals = Arrays.asList('a','e','i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
		String s = SysInHandler.get(String.class, "Bitte Satz eingeben: ");
		char[] chars = s.toCharArray();
		int i = chars.length;
		for (char c : chars)
			if (vocals.contains(c))
				i += 2;
		char[] result = new char[i];
		i = 0;
		for(char c : chars) {
			if (vocals.contains(c)) {
				result[i] = c;
				result[i+1] = 'b';
				i += 2;
			}
			result[i] = c;
			i++;
		}
		System.out.println(String.valueOf(result));
	}

}
