package ueb8;

public class Aufgabe3 {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Invalid ammount of arguments!");
			return;
		}
		try {
			System.out.println(Double.parseDouble(args[0]) * Double.parseDouble(args[1]));
		} catch (Exception e) {
			System.out.println(Double.NaN);
		}
	}

}
