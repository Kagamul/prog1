package ueb8;

import java.util.ArrayList;
import java.util.List;

public class Aufgabe4 {

	public static final String[] phrases = {
		"Phrase1",
		"Phrase2",
		"Phrase3",
		"Phrase4"
	};
	
	public static void main(String[] args) {
		String phrase = phrases[(int)(Math.random()*phrases.length)].toUpperCase();
		List<Character> chars = new ArrayList<Character>();
		String input;
		for (int i = 0; i < 15; i++) {
			System.out.print((i+1)+". Versuch: ");
			if(printPhrase(phrase, chars)) {
				System.out.println("Correct answer!");
				return;
			}
			input = "";
			while (input.length() == 0)
				input = SysInHandler.get(String.class, "Enter a Character: ");
			if(phrase.equalsIgnoreCase(input)) {
				System.out.println("Correct answer!");
				return;
			}
			chars.add(input.toUpperCase().toCharArray()[0]);
			System.out.println(chars);
		}
	}
	
	private static boolean printPhrase(String phrase, List<Character> chars) {
		boolean ret = true;
		for (char c : phrase.toCharArray()) {
			if (c < 91 && c > 64)
				if (chars.contains(c))
					System.out.print(c);
				else {
					System.out.print('_');
					System.out.print(' ');
					ret = false;
				}
			else
				System.out.print(c);
		}
		System.out.println();	
		return ret;
	}

}
