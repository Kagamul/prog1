package ueb8;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SysInHandler {
	
	private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	@SuppressWarnings("unchecked")
	public static <T> T get(Class<T> cls, String ask) {
		
		System.out.print(ask+" ");
		T ret = null;
		try {
			if (cls == Integer.class)
				ret = (T)(Integer)Integer.parseInt(br.readLine());
			else if (cls == Double.class)
				ret = (T)(Double)Double.parseDouble(br.readLine());
			else
				ret = (T) br.readLine();
		} catch (Exception e) {
			System.out.println("Invalid Input!");
			ret = get(cls, ask);
		}
		return ret;
	}
	
	
}
